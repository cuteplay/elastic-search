package com.pl.es.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pl.es.mapper.StudentMapper;
import com.pl.es.po.Student;
import com.pl.es.service.MysqlDataService;
import org.springframework.stereotype.Service;

/**
 * author: pulei2 <br>
 * date: 2020/10/13 16:56 <br>
 */
@Service
public class MysqlDataServiceImpl extends ServiceImpl<StudentMapper, Student> implements MysqlDataService {
}
