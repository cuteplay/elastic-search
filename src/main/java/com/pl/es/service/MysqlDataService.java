package com.pl.es.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pl.es.po.Student;

public interface MysqlDataService extends IService<Student> {
}
