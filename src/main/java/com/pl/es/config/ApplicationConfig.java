package com.pl.es.config;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * author: pulei2 <br>
 * date: 2020/9/14 17:49 <br>
 * description: 配置类 <br>
 */
@Configuration
public class ApplicationConfig {

    @Value("${elasticsearch.cluster.name}")
    private String esClusterName;


    @Value("${elasticsearch.server.transport.urls}")
    private String serverTransportUrls;

    private static final String CLUSTER_NAME_PREFIX = "cluster.name" ;
    private static final String SERVER_ADDRESSES_SPLITTER = ",";
    private static final String ADDRESS_IP_PORT_SPLITTER = ":";

    @Bean
    public TransportClient transportClient() throws UnknownHostException {

        Settings settings = Settings.builder().put(CLUSTER_NAME_PREFIX, esClusterName).build();
        PreBuiltTransportClient preBuiltTransportClient = new PreBuiltTransportClient(settings);
        TransportClient client = new PreBuiltTransportClient(settings);
        for (String addressStr : serverTransportUrls.split(SERVER_ADDRESSES_SPLITTER)) {
            String[] address = addressStr.split(ADDRESS_IP_PORT_SPLITTER);
            client.addTransportAddress(
                    new TransportAddress(InetAddress.getByName(address[0]), Integer.parseInt(address[1])));
        }
        return client;

    }
}
