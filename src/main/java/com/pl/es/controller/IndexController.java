package com.pl.es.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * author: pulei2 <br>
 * date: 2020/10/13 16:48 <br>
 * description: todo <br>
 */
@Controller
@RequestMapping
public class IndexController {

    @RequestMapping
    @ResponseBody
    public String index(){
        return "hello!";
    }
}
