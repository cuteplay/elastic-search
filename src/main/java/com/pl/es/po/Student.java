package com.pl.es.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * author: pulei2 <br>
 * date: 2020/10/13 17:46 <br>
 */
@Data
@TableName("student")
public class Student extends Model<Student> {

    /**id*/
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**姓名*/
    private String name;
    /**年龄*/
    private Integer age;
    /**创建时间*/
    private LocalDateTime createTime;
}
