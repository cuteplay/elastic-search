package com.pl.es.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pl.es.po.Student;
import org.apache.ibatis.annotations.Mapper;

/**
 * author: pulei2 <br>
 * date: 2020/10/14 17:40 <br>
 */
@Mapper
public interface StudentMapper extends BaseMapper<Student> {
}
